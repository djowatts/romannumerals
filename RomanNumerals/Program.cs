﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RomanNumerals
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Roman Numerals Converter");
            Console.WriteLine("-------------------------------------------");
            RunConverter();
        }

        private static void RunConverter()
        {
            var number = GrabInput();
            var romanNumerals = Calculate(number);
            Console.WriteLine("{0} represented as roman numerals is {1}", number, romanNumerals);
            Console.WriteLine();
            Console.WriteLine("Enter 'r' to run again or any other key to quit");
            var input = Console.ReadLine();
            if (input == "r" || input == "R")
            {
                RunConverter();
            }
            Environment.Exit(0);
        }


        private static string Calculate(int input)
        {
            var result = "";
            var numerals = new Dictionary<int, string>() { { 1000, "M" }, { 500, "D" }, { 100, "C" },{ 50, "L" }, { 10, "X" },{9,"XI"}, { 5, "V" },{4, "IV"}, { 1, "I" } };
            var chunks = numerals.Keys.OrderByDescending(x=> x).ToList();
            
            foreach (var chunk in chunks.Where(chunk => input/chunk > 0))
            {
                var split = input/chunk;

                if (chunk == 1000)
                {
                    for (var i = 0; i < split; i++)
                    {
                        result = result + numerals[chunk];
                    }
                }
                else
                {
                    if (split <= 3)
                    {
                        for (var i = 0; i < split; i++)
                        {
                            result = result + numerals[chunk];
                        }
                    }
                    else
                    {
                        var lastItemAdded = result.ToCharArray().Last().ToString();
                        
                        var nextUp = chunks[chunks.IndexOf(chunk) - 1];

                        if (numerals.First(x => x.Value == lastItemAdded).Key > chunk)
                        {
                            result = result.Remove(result.Length - 1);
                        }
                        result = result + numerals[chunk] + numerals[nextUp];
                    }
                   
                }
                input = input - (chunk*split);
            }

            return result;
        }

        private static int GrabInput()
        {
            int number;
            while (true)
            {
                Console.WriteLine("Please enter the number you wish to convert (1 - 4999): ");
                var numberString = Console.ReadLine();
                if (!Int32.TryParse(numberString, out number))
                {
                    var input = RetryInput("That is not a valid number, press q to quit or any other key to continue");
                    if (QuitCheck(input)) continue;
                }
                break;
            }

            if (number < 1 || number > 4999)
            {
                var retriedInput = RetryInput(
                    "That Number is out of bounds, please enter another number between 1 and 4999, or enter q to exit");
                if (QuitCheck(retriedInput)) GrabInput();
            }

            return number;
        }

        private static bool QuitCheck(string input)
        {
            if (input == "q" || input == "Q")
            {
                Environment.Exit(0);
            }
            else
            {
                return true;
            }
            return false;
        }

        private static string RetryInput(string message)
        {
            Console.WriteLine("That is not a valid number, press q to quit or any other key to continue");
            var input = Console.ReadLine();
            return input;
        }
    }
}
